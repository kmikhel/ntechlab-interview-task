#pragma once

#include "WorkerInterface.hpp"
#include "TransposeAgentOpenCL.hpp"


class TransposeWorker : public WorkerInterface {
    TransposeAgentOpenCL transpose_agent;

public:
    ~TransposeWorker() override = default;
    
    std::future<Matrix> AsyncProcess(Matrix matrix) override {
        std::promise<Matrix> result_promise;

        if (!transpose_agent.initialize_cl_kernel()) {
            std::cerr << "OpenCL transpose agent was not properly initialized\n";
            exit(1);
        }
        result_promise.set_value(transpose_agent.transpose(matrix));

        return result_promise.get_future();
    }
};