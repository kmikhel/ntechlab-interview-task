__kernel void transpose(__global int* data_in,
                        __global int* data_out,
                        const int height,
                        const int width) {
    
    unsigned int global_id_x = get_global_id(0);
    unsigned int global_id_y = get_global_id(1);

    if (global_id_x < width && global_id_y < height) {
        data_out[global_id_y + height * global_id_x] = data_in[global_id_x + width * global_id_y];
    }
}