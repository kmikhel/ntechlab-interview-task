#pragma once

#include <vector>


struct Matrix {
    std::vector<int> data;
    unsigned height, width;
};