#### О проекте
Имплементация класса, который выполняет асинхронное транспонирование матриц с использованием 
фреймворка вычислений на GPU OpenCL.

#### Установка пакета OpenCL (Ubuntu)
`sudo apt update`\
`sudo apt install ocl-icd-opencl-dev`

#### Установка драйверов OpenCL (Ubuntu 22.04)

1. Создать временную директорию 

`mkdir neo`

2. Установить `*.deb` пакеты
`cd neo`\
`wget https://github.com/intel/intel-graphics-compiler/releases/download/igc-1.0.14828.8/intel-igc-core_1.0.14828.8_amd64.deb`\
`wget https://github.com/intel/intel-graphics-compiler/releases/download/igc-1.0.14828.8/intel-igc-opencl_1.0.14828.8_amd64.deb`\
`wget https://github.com/intel/compute-runtime/releases/download/23.30.26918.9/intel-level-zero-gpu-dbgsym_1.3.26918.9_amd64.ddeb`\
`wget https://github.com/intel/compute-runtime/releases/download/23.30.26918.9/intel-level-zero-gpu_1.3.26918.9_amd64.deb`\
`wget https://github.com/intel/compute-runtime/releases/download/23.30.26918.9/intel-opencl-icd-dbgsym_23.30.26918.9_amd64.ddeb`\
`wget https://github.com/intel/compute-runtime/releases/download/23.30.26918.9/intel-opencl-icd_23.30.26918.9_amd64.deb`\
`wget https://github.com/intel/compute-runtime/releases/download/23.30.26918.9/libigdgmm12_22.3.0_amd64.deb`\

3. Инсталляция пакетов
`sudo apt install ./*.deb`

#### Компиляция

- Компиляция с помощью clang:

`$ clang++ -std=c++17 -lpthread -lOpenCL main.cpp -o main`

