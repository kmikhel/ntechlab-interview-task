#include <iostream>
#include <thread>
#include "TransposeWorker.hpp"


std::shared_ptr<WorkerInterface> get_new_worker() {
    return std::make_shared<TransposeWorker>();
}


void print_matrix(const Matrix& matrix) {
    std::cout << "Matrix of shape: (" << matrix.height << ", " << matrix.width << ")\n";
    for (unsigned i = 0; i < matrix.height; ++i) {
        for (unsigned j = 0; j < matrix.width; ++j) {
            std::cout << matrix.data[i * matrix.width + j] << ' ';
        }
        std::cout << '\n';
    } 
}


int main() {

    auto worker = get_new_worker();
    auto worker_A = worker;
    auto worker_B = worker;

    Matrix matrix {
        .data = {
            1, 2, 3
        },
        .height = 1,
        .width = 3
    };

    auto transposed = worker->AsyncProcess(matrix);
    
    std::future<Matrix> transposed_A, transposed_B;

    std::thread thread_A([worker_A, &transposed_A]() {
        Matrix matrix {
            .data = {
                1, 2, 3, 4,
                5, 6, 7, 8
            },
            .height = 2,
            .width = 4
        };
        transposed_A = worker_A->AsyncProcess(matrix);
    });

    std::thread thread_B([worker_B, &transposed_B]() {
        Matrix matrix {
            .data = {
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12
            },
            .height = 3,
            .width = 4
        };
        
        transposed_B = worker_B->AsyncProcess(matrix);
    });


    for (int i = 0; i < 3; ++i) {
        std::cout << "Hey, listen!\n";
    }

    thread_A.join();
    thread_B.join();
    
    print_matrix(transposed.get());
    print_matrix(transposed_A.get());
    print_matrix(transposed_B.get());

    return 0;
}
