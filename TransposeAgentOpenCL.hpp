#pragma once 

#include "Matrix.hpp"
#include <CL/opencl.hpp>
#include <vector>
#include <iostream>
#include <fstream>


class TransposeAgentOpenCL {
    cl::Context context;
    cl::CommandQueue command_queue;
    cl::Kernel kernel;

public:
    TransposeAgentOpenCL() {}

    bool initialize_cl_kernel() {
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);

        if (platforms.empty()) {
            std::cerr << "OpenCL platforms not found\n";
            return false;
        }

        auto platform = platforms.front();
        
        std::vector<cl::Device> devices;
        platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
        if (devices.empty()) {
            std::cerr << "GPU devices not found\n";
            return false;
        }     

        auto device = devices.front();
        context = cl::Context(device);
        command_queue = cl::CommandQueue(context, device);

        std::ifstream kernel_file("ocl_kernels/transpose_kernel.cl");
        if (!kernel_file.is_open()) {
            std::cerr << "Failed to open kernel file\n";
            return false;
        }

        std::string kernel_source((std::istreambuf_iterator<char>(kernel_file)),
                                  std::istreambuf_iterator<char>());

        auto program = cl::Program(context, kernel_source);
        if (program.build({device}) != CL_SUCCESS) {
            std::cerr << "Error loading kernel: "
                      << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device)
                      << '\n';
            return false;
        }

        kernel = cl::Kernel(program, "transpose");

        return true;
    }

    Matrix transpose(const Matrix& matrix) {
        Matrix transposed {
            .data = std::vector<int>(matrix.data.size()),
            .height = matrix.width,
            .width = matrix.height
        };

        auto data_in = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                  sizeof(int) * matrix.data.size(), const_cast<int*>(matrix.data.data()));
        auto data_out = cl::Buffer(context, CL_MEM_WRITE_ONLY,
                                   sizeof(int) * matrix.data.size());

        kernel.setArg(0, data_in);
        kernel.setArg(1, data_out);
        kernel.setArg(2, matrix.height);
        kernel.setArg(3, matrix.width);

        command_queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(matrix.width, matrix.height));
        command_queue.enqueueReadBuffer(data_out, CL_TRUE, 0, sizeof(int) * matrix.data.size(), transposed.data.data());

        return transposed;
    }
};