#pragma once

#include "Matrix.hpp"
#include <future>


class WorkerInterface {
public:
    WorkerInterface(WorkerInterface&&) = delete;
    WorkerInterface(const WorkerInterface&) = delete;
    WorkerInterface& operator=(WorkerInterface&&) = delete;
    WorkerInterface& operator=(const WorkerInterface&) = delete;

    WorkerInterface() = default;
    virtual ~WorkerInterface() = default;
    virtual std::future<Matrix> AsyncProcess(Matrix) = 0;
};